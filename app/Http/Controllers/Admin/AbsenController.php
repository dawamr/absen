<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AbsenController extends Controller
{
    public function index()
    {
        return view('admin.absen.index');
    }

    public function showPeformance()
    {
        return view('admin.absen.show_peformance');
    }
}
