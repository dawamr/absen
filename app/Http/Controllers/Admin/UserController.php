<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index()
    {
        return view('admin.user');
    }

    public function store(Request $request)
    {
        if($this->checkPermission('user.create')) abort(404);
        if(!$this->checkRole($request->role)) abort(404);

        DB::beginTransaction();
        try{
            $validator = $this->validator($request->all());

            if($validator->fails()){
                return response()->json(['status' => 'warning', 'msg' => $validator->errors()->first()]);
            }

            $data = [
                'username' => $request->username,
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'phone' => $request->phone,
            ];

            $insert = User::create($data);

            $role = Role::find($request->role);

            if(!$role) {
                return response()->json(['status' => 'warning', 'msg' => 'Role tidak ditemukan']);
            }

            $insert->assignRole($role->name);

            DB::commit();
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['status' => 'success', 'msg' => $e->getMessage()]);
        }

        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan pengguna']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan pengguna']);
    }

    public function data()
    {
        if($this->checkPermission('user.view')) abort(404);

        $users = User::all();

        return DataTables::of($users)
                    ->addColumn('action', function($user) {
                        $action = "";

                        $user_role = $user->roles->first()->name;

                        if($user_role == 'developer' && auth()->user()->roles->first()->name != 'developer'){
                            $action .= "";
                        } else {
                            if(auth()->user()->can('user.manage')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-success' tooltip='Mengelola Pengguna' data-id='{$user->id}' onclick='getManageUser(this);'><i class='fas fa-tasks'></i></a>&nbsp;";
                            if(auth()->user()->can('user.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Memperbarui Pengguna' data-id='{$user->id}' onclick='getUpdateUser(this);'><i class='far fa-edit'></i></a>&nbsp;";
                            if(auth()->user()->can('user.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Pengguna' data-id='{$user->id}' onclick='deleteUser(this);'><i class='fas fa-trash'></i></a>&nbsp;";
                        }

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    protected function validator(array $data, $type = 'insert')
    {
        $username = 'unique:users,username';
        $email = 'unique:users,email';
        $password = ['string', 'min:8', 'max:191', 'confirmed'];
        $phone = 'unique:users,phone';

        if($type == 'update'){
            $username = '';
            $email = '';
            $password = [];
            $phone = '';
        }

        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
            'min' => ':attribute minimal :min karakter',
            'max' => ':attribute maksimal :max karakter',
            'apha_num' => ':attribute tidak boleh ada spasi',
            'unique' => ':attribute sudah terdaftar',
            'email' => ':attribute tidak valid',
            'confirmed' => ':atrribute yang dimasukkan tidak sama',
            'numeric' => ':atrribute harus berupa angka',
        ];

        return Validator::make($data, [
            'username' => ['required', 'string', 'min:6', 'max:191','alpha_num', $username],
            'name' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191', $email],
            'password' => $password,
            'phone' => ['required', 'string', 'numeric', 'max:191', $phone],
            'role' => ['required', 'string', 'max:191'],
        ], $message);
    }

    protected function checkRole($role)
    {
        if($role == '1' && auth()->user()->roles->first()->id != '1') return false;

        return true;
    }

    protected function checkPermission($permission)
    {
        return (bool) (!auth()->user()->can($permission));
    }
}
