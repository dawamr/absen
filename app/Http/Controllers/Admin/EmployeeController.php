<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class EmployeeController extends Controller
{
    public function index(){
        return view('admin.employee.index');
    }

    public function data(){
        $data['data'] = [
            'DT_RowIndex' => 1,
            'name' => 'alan',
            'guard_name' => 'alan',
            'role' => 'alan'
        ];

        return response()->json($data);
    }
}