<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'username' => 'rioprastiawan',
            'name' => 'Rio Prastiawan',
            'email' => 'akunviprio@gmail.com',
            'password' => bcrypt('12345678'),
            'phone' => '628990125338',
        ];

        $create_user = User::create($user);
        $create_user->assignRole('developer');

    }
}
